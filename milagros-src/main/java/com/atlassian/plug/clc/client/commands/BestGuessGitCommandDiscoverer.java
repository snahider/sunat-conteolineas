package com.atlassian.plug.clc.client.commands;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;

public class BestGuessGitCommandDiscoverer implements GitCommandDiscoverer {
	private static final Log log = LogFactory.getLog(BestGuessGitCommandDiscoverer.class);

    public static final String DEFAULT_GIT_EXE = "C:\\Program Files\\Git\\"; //"/opt/local/bin/git";
    public static final String GIT_HOME = "GIT_HOME";
    private CommandExecutor executor;

    public BestGuessGitCommandDiscoverer(CommandExecutor executor) {
        this.executor = executor;
    }

    public String gitCommand() {
        return discoverLocationOfGitCommandLineBinary();
    }

    private String discoverLocationOfGitCommandLineBinary() {
        if (System.getProperty(GIT_HOME) != null) {
            return System.getProperty(GIT_HOME);
        }

        String gitPath = whichGitIsInThePath();
        if (StringUtils.isNotBlank(gitPath)) {
            return gitPath.trim();
        }

        return DEFAULT_GIT_EXE;
    }

    private String whichGitIsInThePath() {
        try {
            WhichCommand whichCommand = new ExecutorWhichCommand(executor);
            return whichCommand.which("git");
        } catch (IOException e) {
            log.error("Failed to execute the git command", e);
            return null;
        }
    }
}
