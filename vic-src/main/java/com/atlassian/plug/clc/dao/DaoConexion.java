package com.atlassian.plug.clc.dao;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;
public class DaoConexion {
	private Connection cnbamboo=null;
	public Connection getConnection() throws NamingException, SQLException{
		Connection cnx =null;
		Context ctx = new InitialContext();
		Context ctxenv = (Context) ctx.lookup("java:comp/env");

		DataSource ds = (DataSource)ctxenv.lookup("jdbc/dsCLC");
		cnx = ds.getConnection();
		return cnx;
	}
	public void conectar() {
		try{
			cnbamboo=this.getConnection();
		}catch (NamingException e ){
			e.printStackTrace();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	public Connection getCnbamboo() {
		return cnbamboo;
	}
	public void setCnbamboo(Connection cnbamboo) {
		this.cnbamboo = cnbamboo;
	}
	public void close() throws SQLException{
		if (this.cnbamboo!=null){
			this.cnbamboo.close();
		}
	}
}
