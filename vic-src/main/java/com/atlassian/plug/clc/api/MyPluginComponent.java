package com.atlassian.plug.clc.api;

public interface MyPluginComponent
{
    String getName();
}