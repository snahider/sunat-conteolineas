package com.atlassian.plug.clc.client.commands;

import java.io.IOException;

public interface WhichCommand {
	 String which(String command) throws IOException;
}
