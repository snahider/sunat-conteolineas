package com.atlassian.plug.clc.client.commands;

public interface GitCommandDiscoverer {
	
	    String gitCommand();
	
}
