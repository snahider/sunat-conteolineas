package com.atlassian.plug.clc;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.netbeans.lib.cvsclient.command.BuildableCommand;
import org.shaded.eclipse.jgit.api.GitCommand;
import org.tmatesoft.svn.core.io.SVNRepository;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.commit.CommitContext;
import com.atlassian.bamboo.commit.CommitFile;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.RepositoryDefinition;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildRepositoryChanges;
import com.atlassian.bamboo.v2.build.repository.RepositoryV2;
import com.atlassian.plug.clc.client.commands.AntCommandExecutor;
import com.atlassian.plug.clc.client.commands.CommandExecutor;
import com.atlassian.plug.clc.dao.DaoConexion;
import com.atlassian.bamboo.plugins.git.GitHubRepository;
import com.atlassian.bamboo.plugins.git.GitOperationHelper;
import com.atlassian.bamboo.plugins.git.GitRepository;


public class CountTask implements TaskType
{
	private final static String SQL_INSERT ="INSERT INTO T7007METRLINCOD (NOM_APLICACION,NOM_LENGUAJE,"+
			"FEC_EJECUCION,NUM_PASE_PROD,NUM_LIN_ANA,NUM_LIN_ELI,NUM_LIN_MOD, " +
			"COD_REPOSITORIO,COD_USUREGIS,FEC_REGIS,COD_USUMODIF,FEC_MODIF) " +
			"VALUES (?,?,CURRENT_DATE,?,?,?,?,?"+
			",?,CURRENT_DATE,?,CURRENT_DATE)";
    @Override
    public TaskResult execute(final TaskContext taskContext) throws TaskException
    {
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		buildLogger.addBuildLogEntry("PLUGIN CLC 1");
		String nomapp="";
		String nomlen="";
		String numpase="";
		int linana=0;
		int lineli=0;
		int linmod=0;
		String codrepo="0";
		String user="DEF";
		String previuosVcsRevisionKey="";
		Calendar calendar = Calendar.getInstance();
		numpase = String.valueOf((calendar.getTime().getYear()+1900)).concat(String.valueOf(calendar.getTime().getMonth())).concat(String.valueOf(calendar.getTime().getDay()));
		numpase = numpase.concat(String.valueOf(calendar.getTimeInMillis()));
		numpase=numpase.length()>18?numpase.substring(0, 18):numpase;

		int cambios=0;
		
		BuildContext buildContext = taskContext.getBuildContext();
		buildLogger.addBuildLogEntry("getRelevantRepositoryIds=>"+buildContext.getRelevantRepositoryIds());		
		
		
		for(RepositoryDefinition repoDef: taskContext.getBuildContext().getRepositoryDefinitions() ){
			Repository repo = repoDef.getRepository();
			
			if(repo instanceof GitRepository){
                GitRepository grepo = (GitRepository)repo;
                buildLogger.addBuildLogEntry("GIT "+ grepo.getAccessData().getRepositoryUrl());
                nomapp="GIT";
				try {
					 BuildRepositoryChanges brChanges= grepo.collectChangesSinceLastBuild(taskContext.getBuildContext().getPlanKey(), previuosVcsRevisionKey);
					 List<CommitContext> lstChanges= brChanges.getChanges();
					 if (lstChanges.size() > 0){
						 PlanKey planKey = PlanKeys.getPlanKey(buildContext.getPlanKey());
						 buildLogger.addBuildLogEntry("planKey=>> "+buildContext.getPlanKey());
						 CommandExecutor cmd = new AntCommandExecutor();
	
						 File fsource = grepo.getSourceCodeDirectory(planKey);
	
						 buildLogger.addBuildLogEntry("PATH=>> "+fsource.getAbsolutePath());
						 File fsource2 = new File("C:\\Users\\Graciano\\bamboo-home-oracle\\xml-data\\build-dir\\_git-repositories-cache\\a040a0ad6660c787dc5a6ab51d37e2929e47a812"); 
						 String str=cmd.execute(new String[]{"git", "log","--shortstat"},fsource2 );
						 buildLogger.addBuildLogEntry("DIFF=>> "+str);
						 //String str=cmd.execute(new String[]{"git", "log","--oneline"},fsource2 );
					 }
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
			}else if (repo instanceof GitHubRepository){
				nomapp="GITHUB";
				GitHubRepository ghrepo = (GitHubRepository)repo;
				buildLogger.addBuildLogEntry("GITHUB" + ghrepo.getAccessData().getRepository());
				try {
					 BuildRepositoryChanges brChanges= ghrepo.collectChangesSinceLastBuild(taskContext.getBuildContext().getPlanKey(), previuosVcsRevisionKey);
					 PlanKey planKey = PlanKeys.getPlanKey(buildContext.getPlanKey());
					 buildLogger.addBuildLogEntry("planKey=>> "+buildContext.getPlanKey());
					 CommandExecutor cmd = new AntCommandExecutor();

					 File fsource = ghrepo.getSourceCodeDirectory(planKey);
							 //.getSourceCodeDirectory(planKey);
					 buildLogger.addBuildLogEntry("PATH=>> "+fsource.getAbsolutePath());
					 String str=cmd.execute(new String[]{"git", "log","--oneline"},fsource );
					 buildLogger.addBuildLogEntry("DIFF=>> "+str);
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(repo instanceof SVNRepository){
				SVNRepository svrepo = (SVNRepository)repo;
				nomapp="SVN";
				buildLogger.addBuildLogEntry("SVN" + svrepo.getRepositoryRoot().getURIEncodedPath());
			}
		}		
/**
 * Conexion  a BD
 */
		DaoConexion dao=null;
		PreparedStatement stmt = null;
		try{
			 dao = new DaoConexion();
			dao.conectar();
		}catch(Exception e){
			e.printStackTrace();
		}


		//List<CommitContext> lstChanges= buildContext.getBuildChanges().getChanges();
		/*Iterator<BuildRepositoryChanges> itRepos = (Iterator<BuildRepositoryChanges>) buildContext.getBuildChanges().getRepositoryChanges();
		while (itRepos.hasNext()){
			BuildRepositoryChanges rChg = itRepos.next();
			List<CommitContext> lstChanges= rChg.getChanges();
			for(CommitContext ct : lstChanges){
	
			}
			
		}*/
		
		/*if(lstChanges!=null && lstChanges.size()>0){
			cambios = lstChanges.size();
			buildLogger.addBuildLogEntry("CLC: Total de cambios =>"+lstChanges.size());
			buildLogger.addBuildLogEntry("CLC: ID de Integracion =>"+buildContext.getBuildChanges().getIntegrationRepositoryId());
			for(CommitContext ct : lstChanges){
				buildLogger.addBuildLogEntry("CLC: =======CHANGES==========================");
				buildLogger.addBuildLogEntry("CLC: FECHA =>"+ct.getDate());
				
				//buildLogger.addBuildLogEntry("CLC: NOMBRE DE AUTOR =>"+ct.getAuthorContext().getName());
				try{
					if(ct.getAuthorContext()!=null){
						user = ct.getAuthorContext().getName();
						user = user.length()>19?user.substring(0,19):user;
					}
				}catch (Exception e){
					user="DEFA";
				}

				List<CommitFile> lstChangeFiles = ct.getFiles();
				if(lstChangeFiles!=null && lstChangeFiles.size()> 0){
					for(CommitFile cf : lstChangeFiles){

						buildLogger.addBuildLogEntry("CLC: ARCHIVO REVISION:"+ cf.getRevision());
						buildLogger.addBuildLogEntry("CLC: ARCHIVO NOMBRE:"+ cf.getName());

					}
				}

				buildLogger.addBuildLogEntry("CLC: //// ====FIN CHANGES============================= ///");
			}
		}*/
		if (cambios > 0){
			nomapp=nomapp.length()>50?nomapp.substring(0,50):nomapp;
			nomlen=nomlen.length()>20?nomlen.substring(0,20):nomlen;
			linana = cambios;
			try{
				stmt=dao.getCnbamboo().prepareStatement(SQL_INSERT);
				stmt.setString(1, nomapp);
				stmt.setString(2, nomlen);
				stmt.setString(3, numpase);
				stmt.setInt(4, linana);
				stmt.setInt(5, lineli);
				stmt.setInt(6, linmod);
				stmt.setString(7, codrepo);
				stmt.setString(8, user);
				stmt.setString(9, user);
				stmt.executeUpdate();
				stmt.close();
				dao.getCnbamboo().commit();
			}catch (SQLException e){
				e.printStackTrace();
			}finally {
		        //if (stmt != null) { stmt.close(); }
		    }
		}

		return TaskResultBuilder.newBuilder(taskContext).success().build();
    }


}