package com.atlassian.plug.clc.configuration;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;

public class PlugConfiguration extends AbstractTaskConfigurator {
	public Map<String, String> generateTaskConfigMap( final ActionParametersMap params, final TaskDefinition previousTaskDefinition)
	{
	    final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);

	    config.put("git_url_local", params.getString("git_url_local"));

	    return config;
	}
	public void validate( final ActionParametersMap params,  final ErrorCollection errorCollection)
	{
	    super.validate(params, errorCollection);

	    final String sayValue = params.getString("git_url_local");
	    if (StringUtils.isEmpty(sayValue))
	    {
	        //errorCollection.addError("git_url_local", textProvider.getText("pcountlc.git_url_local"));
	    }
	}
	@Override
	public void populateContextForCreate(final Map<String, Object> context)
	{
	    super.populateContextForCreate(context);

	    context.put("git_url_local", "");
	}
	
	@Override
	public void populateContextForEdit(final Map<String, Object> context, final TaskDefinition taskDefinition)
	{
	    super.populateContextForEdit(context, taskDefinition);

	    context.put("git_url_local", taskDefinition.getConfiguration().get("git_url_local"));
	}

	@Override
	public void populateContextForView(final Map<String, Object> context,  final TaskDefinition taskDefinition)
	{
	    super.populateContextForView(context, taskDefinition);
	    context.put("git_url_local", taskDefinition.getConfiguration().get("git_url_local"));
	}
}
